Django==1.8.6
dnspython==1.12.0
lxml==3.5.0
pyasn1-modules==0.0.8
pyasn1==0.1.9
pygeoip==0.3.2
six==1.10.0
sleekxmpp==1.3.1
